import * as mongoose from "mongoose";
import AcaoModel from './AcaoModel';
class AcaoDAO{
    public daoMongo:mongoose.Model<mongoose.Document> = AcaoModel;
}

export default new AcaoDAO();