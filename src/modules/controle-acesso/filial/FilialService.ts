import { Data } from './../../../util/Data';
import { msgFilial } from './../../../util/MessageTranslate';
import { oMsg, tMsg } from './../../../util/Values';
import FilialDAO from "./FilialDAO";

class FilialService{

    public async pagination(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            let query = FilialDAO.daoMongo.find(search);
            data.createPagination(req, query);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoListarFiliais);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAllFilter(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            data.list = await FilialDAO.daoMongo.find(search);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoListarFiliais);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAll(req, res){
        let data = new Data();
        try {
            data.list = await FilialDAO.daoMongo.find({});
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoListarFiliais);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let filial = req.body;
            data.obj = await FilialDAO.daoMongo.create(filial);
            data.addMsg(tMsg.SUCCESS, msgFilial.filialInseridaComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoInserirFilial);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            let filial = req.body;
            data.obj = await FilialDAO.daoMongo.update(id, filial);
            data.addMsg(tMsg.SUCCESS, msgFilial.filialAtualizadaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoAtualizarFilial);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            await FilialDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgFilial.filialRemovidaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgFilial.erroAoRemoverFilial);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await FilialDAO.daoMongo.findById(id);
        } catch (error) {
            data[tMsg.DANGER] = msgFilial.erroAoLocalizarFilial;
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new FilialService();