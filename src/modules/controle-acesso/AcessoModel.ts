import * as mongoose from "mongoose";

export const AcessoSchema = new mongoose.Schema({
    login:{
        type: String,
        unique: true,
        index: true,
        required: true,
        trim: true
    },
    senha:{
        type: String, required: true
    },
    nome:{
        type: String
    },
    isAdm: {
        type: Boolean, default: false
    },
    perfis: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Perfil"
    }]
});

export default mongoose.model("Acesso", AcessoSchema);