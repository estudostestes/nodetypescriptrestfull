export const msgAuth = {//Mensagens de retornos
    "erroAoConsultarLoginDeAcesso" : "ERRO_AO_CONSULTAR_LOGIN_DE_ACESSO",
    "tokenNaoEnviado": "TOKEN_NAO_ENVIADO",
    "loginInvalido" : "LOGIN_INVALIDO",
    "tokenInvalido" : "TOKEN_INVALIDO",
    "loginhaOuSenhaInvalidos": "LOGIN_OU_SENHA_INVALIDOS",
    "erroAoValidarToken": "ERRO_AO_VALIDAR_TOKEN",
    "nenhumLoginEncontrado": "NENHUM_LOGIN_USUARIO" 
};

export const msgUsuario = {
    "erroAoListarUsuarios": "ERRO_AO_LISTAR_USUARIOS",
    "nenhumUsuarioEncontrado": "NENHUM_USUARIO_ENCONTRADO",
    "erroAoLocalizarUsuario": "ERRO_AO_LOCALIZAR_USUARIO",
    "erroAoInserirUsuario": "ERRO_AO_INSERIR_USUARIO",
    "erroAoAtualizarUsuario": "ERRO_AO_ATUALIZAR_USUARIO",
    "erroAoRemoverUsuario": "ERRO_AO_REMOVER_USUARIO",
    "usuarioInseridoComSucesso": "USUARIO_INSERIDO_COM_SUCESSO",
    "usuarioAtualizadoComSucesso": "USUARIO_ATUALIZADO_COM_SUCESSO",
    "usuarioRemovidoComSucesso": "USUARIO_REMOVIDO_COM_SUCESSO",
}

export const msgOrganizacao = {
    "erroAoListarOrganizacoes": "ERRO_AO_LISTAR_ORGANIZACOES",
    "nenhumOrganizacaoEncontrada": "NENHUMA_ORGANIZACAO_ENCONTRADA",
    "erroAoLocalizarOrganizacao": "ERRO_AO_LOCALIZAR_ORGANIZACAO",
    "erroAoInserirOrganizacao": "ERRO_AO_INSERIR_ORGANIZACAO",
    "erroAoAtualizarOrganizacao": "ERRO_AO_ATUALIZAR_ORGANIZACAO",
    "erroAoRemoverOrganizacao": "ERRO_AO_REMOVER_ORGANIZACAO",
    "organizacaoInseridaComSucesso": "ORGANIZACAO_INSERIDA_COM_SUCESSO",
    "organizacaoAtualizadaComSucesso": "ORGANIZACAO_ATUALIZADA_COM_SUCESSO",
    "organizacaoRemovidaComSucesso": "ORGANIZACAO_REMOVIDA_COM_SUCESSO",
}

export const msgFilial = {
    "erroAoListarFiliais": "ERRO_AO_LISTAR_FILIAIS",
    "nenhumAFilialEncontrada": "NENHUMA_FILIAL_ENCONTRADA",
    "erroAoLocalizarFilial": "ERRO_AO_LOCALIZAR_FILIAL",
    "erroAoInserirFilial": "ERRO_AO_INSERIR_FILIAL",
    "erroAoAtualizarFilial": "ERRO_AO_ATUALIZAR_FILIAL",
    "erroAoRemoverFilial": "ERRO_AO_REMOVER_FILIAL",
    "filialInseridaComSucesso": "FILIAL_INSERIDO_COM_SUCESSO",
    "filialAtualizadaComSucesso": "FILIAL_ATUALIZADO_COM_SUCESSO",
    "filialRemovidaComSucesso": "FILIAL_REMOVIDO_COM_SUCESSO",
}

export const msgPerfil = {
    "erroAoListarPerfis": "ERRO_AO_LISTAR_PERFIS",
    "nenhumPerfilEncontrado": "NENHUM_PERFIL_ENCONTRADO",
    "erroAoLocalizarPerfil": "ERRO_AO_LOCALIZAR_PERFIL",
    "erroAoInserirPerfil": "ERRO_AO_INSERIR_PERFIL",
    "erroAoAtualizarPerfil": "ERRO_AO_ATUALIZAR_PERFIL",
    "erroAoRemoverPerfil": "ERRO_AO_REMOVER_PERFIL",
    "perfilInseridoComSucesso": "PERFIL_INSERIDO_COM_SUCESSO",
    "perfilAtualizadoComSucesso": "PERFIL_ATUALIZADO_COM_SUCESSO",
    "perfilRemovidoComSucesso": "PERFIL_REMOVIDO_COM_SUCESSO",
}

export const msgAcao = {
    "erroAoListarAcoes": "ERRO_AO_LISTAR_ACOES",
    "nenhumAAcaoEncontrada": "NENHUMA_ACAO_ENCONTRADA",
    "erroAoLocalizarAcao": "ERRO_AO_LOCALIZAR_ACAO",
    "erroAoInserirAcao": "ERRO_AO_INSERIR_ACAO",
    "erroAoAtualizarAcao": "ERRO_AO_ATUALIZAR_ACAO",
    "erroAoRemoverAcao": "ERRO_AO_REMOVER_ACAO",
    "acaoInseridaComSucesso": "ACAO_INSERIDO_COM_SUCESSO",
    "acaoAtualizadaComSucesso": "ACAO_ATUALIZADO_COM_SUCESSO",
    "acaoRemovidaComSucesso": "ACAO_REMOVIDO_COM_SUCESSO"
}

export const msgAcesso = {
    "erroAoListarAcessos": "ERRO_AO_LISTAR_ACESSOS",
    "nenhumAAcessoEncontrado": "NENHUMA_ACESSO_ENCONTRADA",
    "erroAoLocalizarAcesso": "ERRO_AO_LOCALIZAR_ACESSO",
    "erroAoInserirAcesso": "ERRO_AO_INSERIR_ACESSO",
    "erroAoAtualizarAcesso": "ERRO_AO_ATUALIZAR_ACESSO",
    "erroAoRemoverAcesso": "ERRO_AO_REMOVER_ACESSO",
    "acessoInseridoComSucesso": "ACESSO_INSERIDO_COM_SUCESSO",
    "acessoAtualizadoComSucesso": "ACESSO_ATUALIZADO_COM_SUCESSO",
    "acessoRemovidoComSucesso": "ACESSO_REMOVIDO_COM_SUCESSO"
}
