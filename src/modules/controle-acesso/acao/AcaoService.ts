import { Data } from './../../../util/Data';
import { msgAcao } from './../../../util/MessageTranslate';
import { oMsg, tMsg } from './../../../util/Values';
import AcaoDAO from "./AcaoDAO";

class AcaoService{

    public async pagination(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            let query = AcaoDAO.daoMongo.find(search);
            data.createPagination(req, query);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoListarAcoes);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAllFilter(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            data.list = await AcaoDAO.daoMongo.find(search);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoListarAcoes);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAll(req, res){
        let data = new Data();
        try {
            data.list = await AcaoDAO.daoMongo.find({});
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoListarAcoes);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let acao = req.body;
            data.obj = await AcaoDAO.daoMongo.create(acao);
            data.addMsg(tMsg.SUCCESS, msgAcao.acaoInseridaComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoInserirAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            let acao = req.body;
            data.obj = await AcaoDAO.daoMongo.update(id, acao);
            data.addMsg(tMsg.SUCCESS, msgAcao.acaoAtualizadaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoAtualizarAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            data.obj = await AcaoDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgAcao.acaoRemovidaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoRemoverAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await AcaoDAO.daoMongo.findById(id);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcao.erroAoLocalizarAcao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new AcaoService();