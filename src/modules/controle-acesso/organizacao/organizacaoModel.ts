import * as mongoose from "mongoose";

export const OrganizacaoSchema = new mongoose.Schema({
    cnpj:{
        type: String,
        unique: true,
        index: true,
        required: true,
        trim: true
    },
    nomeFantasia:{
        type: String,
        unique: true,
        required: true
    },
    filiais: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Perfil"
    }],
    perfis: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Filial"
    }]
});

export default mongoose.model("Organizacao", OrganizacaoSchema);