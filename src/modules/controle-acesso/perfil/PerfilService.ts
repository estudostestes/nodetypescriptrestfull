import { Data } from './../../../util/Data';
import { msgPerfil } from './../../../util/MessageTranslate';
import { oMsg, tMsg } from './../../../util/Values';
import PerfilDAO from "./PerfilDAO";

class PerfilService{

    public async pagination(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            let query = PerfilDAO.daoMongo.find(search);
            data.createPagination(req, query);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoListarPerfis);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAllFilter(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            data.list = await PerfilDAO.daoMongo.find(search);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoListarPerfis);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAll(req, res){
        let data = new Data();
        try {
            data.list = await PerfilDAO.daoMongo.find({});
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoListarPerfis);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let perfil = req.body;
            data.obj = await PerfilDAO.daoMongo.create(perfil);
            data.addMsg(tMsg.SUCCESS, msgPerfil.perfilInseridoComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoInserirPerfil);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            let perfil = req.body;
            data.obj = await PerfilDAO.daoMongo.update(id, perfil);
            data.addMsg(tMsg.SUCCESS, msgPerfil.perfilAtualizadoComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoAtualizarPerfil);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            data.obj = await PerfilDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgPerfil.perfilRemovidoComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoRemoverPerfil);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await PerfilDAO.daoMongo.findById(id);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgPerfil.erroAoLocalizarPerfil);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new PerfilService();