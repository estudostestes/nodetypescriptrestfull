import * as mongoose from "mongoose";
import PerfilModel from './PerfilModel';
class PerfilDAO{
    public daoMongo:mongoose.Model<mongoose.Document> = PerfilModel;
}

export default new PerfilDAO();