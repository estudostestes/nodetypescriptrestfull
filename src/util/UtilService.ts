import { secretToken } from './Values';
import * as crypto from "crypto";

class UtilService {

    private algoritEncrypt = 'aes256';
       
    public encrypt(text: any){
        try {
            let cipher = crypto.createCipher(this.algoritEncrypt, secretToken.secret);
            let encrypted = cipher.update(text, 'utf8', 'hex');
            encrypted += cipher.final('hex');
            return encrypted;
        } catch (error) {
        }
    }
    public decrypt(textEncript: any){
        try {
            let decipher = crypto.createDecipher(this.algoritEncrypt, secretToken.secret);
            console.log("text: " + textEncript);
            let decrypted = decipher.update(textEncript, 'hex', 'utf8');
            decrypted += decipher.final('utf8');
            return decrypted;
        } catch (error) {
            console.log("Erro ao decriptar: " + textEncript);
            return "";
        }
    }
    
    public hMacSha512(text: any){
        try {
            const hmac = crypto.createHmac('sha512', secretToken.secret);
            hmac.update(text);
            return hmac.digest('hex');
        } catch (error) {
            console.log("Erro ao criar hMacSha512: " + text);
            return "";
       }
   }
}

export default new UtilService();