import { Data } from './../../util/Data';
import { msgUsuario } from './../../util/MessageTranslate';
import { oMsg, tMsg} from './../../util/Values';
import UsuarioDAO from "./UsuarioDAO";
import App from "../../app";

class UsuarioService {
    constructor(){}
    
    public async pagination(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            let query = UsuarioDAO.daoMongo.find(search);
            data.createPagination(req, query);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoListarUsuarios);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAllFilter(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            data.list = await UsuarioDAO.daoMongo.find(search);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoListarUsuarios);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAll(req, res){
        let data = new Data();
        try {
            data.list = await UsuarioDAO.daoMongo.find({});
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoListarUsuarios);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let usuario = req.body;
            data.obj = await UsuarioDAO.daoMongo.create(usuario);
            data.addMsg(tMsg.SUCCESS, msgUsuario.usuarioInseridoComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoInserirUsuario);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            let usuario = req.body;
            data.obj = await  UsuarioDAO.daoMongo.update(id,usuario);
            data.addMsg(tMsg.SUCCESS, msgUsuario.usuarioAtualizadoComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoAtualizarUsuario);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            data.obj = await UsuarioDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgUsuario.usuarioRemovidoComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoRemoverUsuario);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await UsuarioDAO.daoMongo.findById(id);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgUsuario.erroAoLocalizarUsuario);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new UsuarioService();