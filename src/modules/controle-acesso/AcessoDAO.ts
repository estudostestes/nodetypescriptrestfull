import * as mongoose from "mongoose";
import AcessoModel from './AcessoModel';
class AcessoDAO{
    public daoMongo:mongoose.Model<mongoose.Document> = AcessoModel;
}

export default new AcessoDAO();