import * as mongoose from "mongoose";
import OrganizacaoModel from './OrganizacaoModel';

class OrganizacaoDAO {
    public daoMongo: mongoose.Model<mongoose.Document>;

    constructor() {
        this.daoMongo = OrganizacaoModel;
    }
}

export default new OrganizacaoDAO();