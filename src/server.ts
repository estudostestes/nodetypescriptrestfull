import App from "./app";
import * as mongoose from "mongoose";
import * as debugServe from "debug";

let port = process.env.PORT || "3000";
let debug = debugServe("nodestr:server");
App.server.listen(port, () =>{
    console.log("Servidor está rodando na porta "+ port);
    App.mongoCreateConnection(() => {
        console.log("Banco conectado com sucesso!");
    }, () => {
        console.log("O banco de dados foi desconectado!");
    }, (error) => {
        console.log("Error ao conectar ao banco de dados: " + error);
    });
});

App.server.on("listening", setDebug);

process.once("SIGUSR2", () => {
    App.mongoCloseConnection(() => {
        console.log("Sistema reiniciado!");
        process.kill(process.pid, "SIGUSR2");
    });
});

process.once("SIGINT", () => {
    App.mongoCloseConnection(() => {
        console.log("Sistema fechado!");
        process.exit(0);
    });
});

function setDebug(){
    let addr = App.server.address();
    let bind = typeof addr === 'string'
    ? "pipe "  + addr
    : "port " + addr.port;
    debug("Listening on " + bind);
    console.log("Debug Iniciado.");
}