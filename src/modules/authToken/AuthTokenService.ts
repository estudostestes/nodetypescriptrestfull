import { msgAuth } from './../../util/MessageTranslate';
import { tMsg, oMsg, secretToken } from './../../util/Values';
import AuthTokenDAO from "./AuthTokenDAO";
import UtilService from "../../util/UtilService"
import * as jwt from "jsonwebtoken";
import { Data } from './../../util/Data';

class AuthTokenService{
    constructor(){}

    public async getLoginAuth(req, res){
        let data = new Data();
        try {
            let login = req.params[AuthTokenDAO.loginName];
            let result = await AuthTokenDAO.getByLogin(login);
            let useAuth= {};
            if(!result){
                data.addMsg(tMsg.DANGER, msgAuth.loginInvalido);
            }else{
                useAuth[AuthTokenDAO.loginName] = result[AuthTokenDAO.loginName];
                useAuth[AuthTokenDAO.name] = result[AuthTokenDAO.name];
            }
            data.obj = useAuth;
        } catch (error) {
            data.addMsg(tMsg.DANGER,  msgAuth.erroAoConsultarLoginDeAcesso);
            console.log("Erro ao tentar consultar login: " + error);
        }finally{
            console.log(data);
            res.status(200).json(data);
        }
    }

    public async authenticate(req, res){
        let data = new Data();
        let result: any;
        try {
            let userLogin = req.body;
            let login = userLogin[AuthTokenDAO.loginName];
            let senha = userLogin[AuthTokenDAO.senhaName];

            result = await AuthTokenDAO.getByLoginSenha(login, senha);
            if(!result){
                data.addMsg(tMsg.DANGER, msgAuth.loginhaOuSenhaInvalidos);
                console.log("Login ou senha inválidos");
                res.sendStatus(401);
            }else{
                let loginToken = result[AuthTokenDAO.loginName];
                let token = jwt.sign({ login: loginToken, iat: secretToken.TIME}, secretToken.SECRET);
                data[secretToken.TOKEN] = token;
                data.obj = result;
            }
            res.json(data);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.erroAoConsultarLoginDeAcesso);
            console.log("Erro ao localizar usuário: " + error);
            res.sendStatus(401);
        }
    }

    public async validateToken(req, res, next){
        let data = new Data();
        try {
            let token = req.headers[secretToken.TOKEN];
            if(token){
                console.log("Validando o token");
                let userLogged = await jwt.verify(token, secretToken.SECRET);
                req[AuthTokenDAO.loginName] = userLogged;
                next();
            }else{
                data.addMsg(tMsg.DANGER, msgAuth.tokenNaoEnviado);
                console.log("Token não foi enviado.");
                res.sendStatus(401);
            }
        } catch (error) {
            console.log("Erro ao validar Token." + error);
        }    
        
    }

    public async getUserLogged(req, res){
        let data = new Data();
        try {
            let userLogged = req[AuthTokenDAO.loginName];
            let login = userLogged["login"];
            if(login){
                console.log("Buscou no banco");
                data.obj = await AuthTokenDAO.getByLogin(login);
            }else{
                data.addMsg(tMsg.DANGER, msgAuth.nenhumLoginEncontrado);
            }
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAuth.erroAoConsultarLoginDeAcesso);
            console.log("Erro ao tentar consultar login: " + error);
        }finally{
            res.status(200).json(data);
            console.log("retornou os dados");
        }
    }
}

export default new AuthTokenService();