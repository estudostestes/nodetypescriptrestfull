import * as mongoose from "mongoose";

export const FilialSchema = new mongoose.Schema({
    cnpj:{
        type: String,
        unique: true,
        index: true,
        required: true,
        trim: true
    },
    nomeFantasia:{
        type: String,
        unique: true,
        required: true
    },
    organizacao: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Organizacao"
    },
    perfis:[{
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Perfil"
    }]
});

export default mongoose.model("Filial", FilialSchema);