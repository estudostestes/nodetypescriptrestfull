import * as express from "express";
import * as http from "http";
import * as morgan from "morgan";
import * as bodyParser from "body-parser";
import * as path from "path";
import MongoDB from "./util/MongoDB";
import SocketUtil from "./util/SocketUtil";
import * as cors from "cors";

//Módulos
import AuthTokenRest from "./modules/authToken/AuthTokenRest";
import  UsuarioRest from './modules/usuario/UsuarioRest';
import OrganizacaoRest from "./modules/controle-acesso/organizacao/OrganizacaoRest";
import FilialRest from "./modules/controle-acesso/filial/FilialRest";
import PerfilRest from "./modules/controle-acesso/perfil/PerfilRest";
import AcaoRest from "./modules/controle-acesso/acao/AcaoRest";
import AcessoRest from "./modules/controle-acesso/AcessoRest";

class App {
    public exp: express.Application;
    public server: http.Server;
    private morgan: morgan.Morgan;
    private bodyParser; 
    private mondoDb: MongoDB;
    private cors;
    
    constructor(){
        this.exp = express();//Criando servidor do Express
        this.server = SocketUtil.getServerSocketIo(this.exp);
        this.middlewares();
        this.mondoDb = new MongoDB();
        this.initRoutes();
    }
    
    public mongoCreateConnection(onConnected: any, onDisconnected: any, onError: any){
        this.mondoDb.createConnection(onConnected, onDisconnected, onError);
    }
    
    public mongoCloseConnection(onClose){
        this.mondoDb.closeConnection(onClose);
    }

    private middlewares(){
        this.exp.use(morgan("dev"));
        this.exp.use(bodyParser.json());
        this.exp.use(bodyParser.urlencoded({extended: true}));
        // this.exp.use(express.static(path.join(__dirname, "public")));
        this.exp.use(cors());
    }

    private initRoutes(){
        AuthTokenRest.setRoutes(this.exp);//Primeiro modulo, para fazer a validações dos acessos.
        OrganizacaoRest.setRoutes(this.exp);
        FilialRest.setRoutes(this.exp);
        PerfilRest.setRoutes(this.exp);
        AcaoRest.setRoutes(this.exp);
        AcessoRest.setRoutes(this.exp);
    }
} 

export default new App();