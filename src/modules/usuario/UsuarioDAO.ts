import * as mongoose from "mongoose";
import Usuario from './UsuarioModel';
class UsuarioDao{
    public daoMongo:mongoose.Model<mongoose.Document> = Usuario;
}

export default new UsuarioDao();