import { oMsg } from './Values';
import * as mongoose from "mongoose";
import * as express from "express";
export class Data{
    constructor(){
        this.listMsg = new Array();
    }
    public obj: Object;
    public list: Array<any>;
    public listMsg: Array<any>;   
    
    public page: number;  
    public totalPages: number;  
    public limit: number;  
    public rangeStart: number;  
    public rangeEnd: number;  
    public fieldSort: string;  
    public sort: string;  
    public totalRows: number;  
    public offset: number; 


    public addMsg(typeMsg: string, textMsg: string){
        this.listMsg.push({type: typeMsg, msg: textMsg});
    }

    public async createPagination(request:express.Request , query: mongoose.Query<any>){
        let obj = request.body;
        this.page = parseInt(obj[oMsg.PAGE]) || 1;
        this.limit = parseInt(obj[oMsg.LIMIT]) || 10;
        this.fieldSort = obj[oMsg.FIELD_SORT] || "_id";
        this.sort = obj[oMsg.SORT] || 'asc';
        let sorting =  {};
        sorting[this.fieldSort] = this.sort;
        this.totalRows = await query.count();
        this.calcRanges();
        let skipe = (this.page-1) * this.limit;
        this.list = await query.skip((skipe)).limit(this.limit).sort(sorting).exec("find");
    }

    public calcRanges(){
        if(this.page > 0 && this.totalRows > 0){
            this.totalPages = Math.ceil(this.totalRows / this.limit);
        }
        if(this.page < this.totalPages){
            this.rangeEnd = this.page * this.limit;
        } else {
            this.rangeEnd = this.totalRows;
            this.page = this.totalPages;
        }
        if(this.page != this.totalPages){
            this.rangeStart = (this.rangeEnd + 1) - this.limit 
        } else{
            this.rangeStart =  ((this.totalPages - 1) * this.limit) + 1;

        }
    }

    public setLikesObject(object: Object){
        for (var key in object) {
            if (object[key]) {
                if(typeof object[key] == "string"){
                    object[key] = { $regex: '.*' + object[key] + '.*' };
                }
            }else{
                delete object[key];
            }
        }
        return object;
    }
}