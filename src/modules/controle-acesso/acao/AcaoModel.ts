import * as mongoose from "mongoose";

export const AcaoSchema = new mongoose.Schema({
    nome:{
        type: String,
        unique: true,
        required: true
    },
    titulo:{
        type: String,
    },
    url:{
        type: String,
    },
    componente:{
        type: String,
    },
    subAcoes: [{
        type:mongoose.SchemaTypes.ObjectId,
        ref: "Acao"
    }]
});

export default mongoose.model("Acao", AcaoSchema);