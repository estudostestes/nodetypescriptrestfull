import * as mongoose from "mongoose";

class AuthTokenDAO{
    private modelName: string = "Acesso";
    public name: string = "nome";
    public loginName: string = "login";
    public senhaName: string = "senha";

    constructor(){ }

    getByLogin(login: string){
       let search = {};
       search[this.loginName] = login;
       return mongoose.model(this.modelName).findOne(search);
    }
    
    getByLoginSenha(login: string, senha: string){
        let search = {};
        search[this.loginName] = login;
        search[this.senhaName] = senha;
        return mongoose.model(this.modelName).findOne(search);
    }
}

export default new AuthTokenDAO();