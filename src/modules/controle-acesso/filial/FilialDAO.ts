import * as mongoose from "mongoose";
import FilialModel from './FilialModel';
class FilialDAO{
    public daoMongo:mongoose.Model<mongoose.Document> = FilialModel;
}

export default new FilialDAO();