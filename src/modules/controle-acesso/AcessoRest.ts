import { URLs } from './../../util/Values';
import  AcessoService from './AcessoService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class AcessoRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.ACESSO)
        .get(AcessoService.getAll)
        .post(AcessoService.create);

        exp.route(URLs.ACESSO_PAGINATION)
        .post(AcessoService.pagination);

        exp.route(URLs.ACESSO_FILTER)
        .post(AcessoService.getAllFilter);
        
        exp.route(URLs.ACESSO_ID)
        .get(AcessoService.getById)
        .delete(AcessoService.remove)
        .put(AcessoService.update);
    }
}

export default new  AcessoRest();