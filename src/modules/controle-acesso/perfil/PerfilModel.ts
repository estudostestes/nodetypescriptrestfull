import * as mongoose from "mongoose";

export const PerfilSchema = new mongoose.Schema({
    nome:{
        type: String,
        unique: true,
        required: true
    },

    descricao:{
        type: String
    },

    organizacao: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: "Organizacao"
    },

    acoes: [{
        type:mongoose.SchemaTypes.ObjectId,
        ref: "Acao"
    }]

});

export default mongoose.model("Perfil", PerfilSchema);