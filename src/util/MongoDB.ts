import * as mongoose from "mongoose";

class MongoDB{
    
    //Dados conexão.
    private url = "localhost";
    private porta = "27017";
    private database = "web-rest-node-beta";
    private usuario  = "";
    private senha = "";
    private uri =  "mongodb://";
    private options = {
        useMongoClient: true
        //,server: { poolSize: 5 }
    };
    private db: any;
    private conn: any;

    constructor(){
        
        
    }
    
    createConnection(onConnected: any, onDisnnected: any, onError: any){
        //mongoose.Promise = global.P;
        console.log("---------------Chegou no MongoDB!");
        let nodeEnv:string = process.env.NODE_ENV;
        console.log("Node_ENV: " + nodeEnv);
        if(!nodeEnv){
            console.log("---------------Ambiente Local");
        }else if(nodeEnv.indexOf("test") > -1){
            //Mongo MLAB
            this.porta = "49575";
            this.usuario = "roger";
            this.senha = "123456789";
            this.database = "estudosdb";
            this.url = this.usuario + ":" + this.senha + "@ds249575.mlab.com";
            //mongodb://<dbuser>:<dbpassword>@ds249575.mlab.com:49575/estudosdb
            console.log("---------------Ambiente de Teste");
        }else if(nodeEnv.indexOf("development") > -1){
            console.log("---------------Ambiente de esenvolvimento!");
        }else if(nodeEnv.indexOf("production") > -1){
            console.log("---------------Ambiente de produção!");
        }
        
        //Montando URI
        this.uri += this.url + ":" + this.porta + "/" + this.database;
        console.log(this.uri);
        //Conectando ao banco
        this.db = mongoose.connect(this.uri, this.options);
        //Conexao
        this.conn = mongoose.connection;

        this.conn.on("connected", onConnected);
        
        this.conn.on("disconnected", onDisnnected);
        
        this.conn.on("error", onError);
    }

    closeConnection(onClose: any){
        if(this.conn){
            this.conn.close(onClose);
        }
    }
}

export default MongoDB;