import { URLs } from './../../../util/Values';
import  OrganizacaoService from './OrganizacaoService';
import * as express from "express";
import SocketUtil from "../../../util/SocketUtil";

class OrganizacaoRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.ORGANIZACAO)
        .get(OrganizacaoService.getAll)
        .post(OrganizacaoService.create);
        
        exp.route(URLs.ORGANIZACAO_PAGINATION)
        .post(OrganizacaoService.pagination);

        exp.route(URLs.ORGANIZACAO_FILTER)
        .post(OrganizacaoService.getAllFilter);

        exp.route(URLs.ORGANIZACAO_ID)
        .get(OrganizacaoService.getById)
        .delete(OrganizacaoService.remove)
        .put(OrganizacaoService.update);
    }
}

export default new  OrganizacaoRest();