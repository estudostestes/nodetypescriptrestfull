import { URLs } from './../../../util/Values';
import  AcaoService from './AcaoService';
import * as express from "express";
import SocketUtil from "../../../util/SocketUtil";

class AcaoRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.ACAO)
        .get(AcaoService.getAll)
        .post(AcaoService.create);

        exp.route(URLs.ACAO_PAGINATION)
        .post(AcaoService.pagination);

        exp.route(URLs.ACAO_FILTER)
        .post(AcaoService.getAllFilter);
        
        exp.route(URLs.ACAO_ID)
        .get(AcaoService.getById)
        .delete(AcaoService.remove)
        .put(AcaoService.update);
    }
}

export default new  AcaoRest();