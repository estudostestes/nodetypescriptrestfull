import { Data } from './../../util/Data';
import { msgAcesso } from './../../util/MessageTranslate';
import { oMsg, tMsg } from './../../util/Values';
import AcessoDAO from "./AcessoDAO";

class AcessoService{
    
    public async pagination(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            let query = AcessoDAO.daoMongo.find(search);
            data.createPagination(req, query);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoListarAcessos);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAllFilter(req, res){
        let data = new Data();
        try {
            let search = req.body[oMsg.OBJ];
            data.setLikesObject(search);
            data.list = await AcessoDAO.daoMongo.find(search);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoListarAcessos);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAll(req, res){
        let data = new Data();
        try {
            data.list = await AcessoDAO.daoMongo.find({});
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoListarAcessos);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let acesso = req.body;
            data.obj = await AcessoDAO.daoMongo.create(acesso);
            data.addMsg(tMsg.SUCCESS, msgAcesso.acessoInseridoComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoInserirAcesso);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            let acesso = req.body;
            data.obj = await  AcessoDAO.daoMongo.update(id, acesso);
            data.addMsg(tMsg.SUCCESS, msgAcesso.acessoAtualizadoComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoAtualizarAcesso);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            data.obj = await AcessoDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgAcesso.acessoRemovidoComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoRemoverAcesso);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await AcessoDAO.daoMongo.findById(id);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgAcesso.erroAoLocalizarAcesso);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new AcessoService();