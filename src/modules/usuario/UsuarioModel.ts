import * as mongoose from "mongoose";

export const UsuarioSchema = new mongoose.Schema({
    nome:{type: String, required: true},
    login:{type: String, required: true},
    senha:{type: String, required: true},
    email:{type: String},
    idade: {type:Number}
});

export default mongoose.model("Usuario", UsuarioSchema);