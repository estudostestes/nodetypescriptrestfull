import { URLs } from './../../../util/Values';
import  FilialService from './FilialService';
import * as express from "express";
import SocketUtil from "../../../util/SocketUtil";

class FilialRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.FILIAL)
        .get(FilialService.getAll)
        .post(FilialService.create);

        exp.route(URLs.FILIAL_PAGINATION)
        .post(FilialService.pagination);

        exp.route(URLs.FILIAL_FILTER)
        .post(FilialService.getAllFilter);
        
        exp.route(URLs.FILIAL_ID)
        .get(FilialService.getById)
        .delete(FilialService.remove)
        .put(FilialService.update);
    }
}

export default new  FilialRest();