import { URLs } from './../../util/Values';
import  UsuarioService from './UsuarioService';
import * as express from "express";
import SocketUtil from "../../util/SocketUtil";

class UsuarioRest{
    constructor(){
        //Definindo listens WebSockets
        SocketUtil.addListen("verifique", (msg) => {
            console.log(msg);
            SocketUtil.io.emit("msgTodos", "Chegou no socket!");
        });
    }
    
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.USUARIO)
        .get(UsuarioService.getAll)
        .post(UsuarioService.create);

        exp.route(URLs.USUARIO_PAGINATION)
        .post(UsuarioService.pagination);
        
        exp.route(URLs.USUARIO_FILTER)
        .post(UsuarioService.getAllFilter);

        exp.route(URLs.USUARIO_ID)
        .get(UsuarioService.getById)
        .delete(UsuarioService.remove)
        .put(UsuarioService.update);
    }
}
export default new UsuarioRest();