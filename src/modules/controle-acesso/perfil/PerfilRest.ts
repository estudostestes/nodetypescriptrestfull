import { URLs } from './../../../util/Values';
import  PerfilService from './PerfilService';
import * as express from "express";
import SocketUtil from "../../../util/SocketUtil";

class PerfilRest{
    public setRoutes(exp: express.Application){
        //Definindo as rotas.
        exp.route(URLs.PERFIL)
        .get(PerfilService.getAll)
        .post(PerfilService.create);

        exp.route(URLs.PERFIL_PAGINATION)
        .post(PerfilService.pagination);

        exp.route(URLs.PERFIL_FILTER)
        .post(PerfilService.getAllFilter);
        
        exp.route(URLs.PERFIL_ID)
        .get(PerfilService.getById)
        .delete(PerfilService.remove)
        .put(PerfilService.update);
    }
}

export default new  PerfilRest();