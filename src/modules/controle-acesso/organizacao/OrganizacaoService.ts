import { Data } from './../../../util/Data';
import { msgOrganizacao } from './../../../util/MessageTranslate';
import { oMsg, tMsg } from './../../../util/Values';
import OrganizacaoDAO from "./OrganizacaoDAO";

class OrganizacaoService{

    public async pagination(req, res) {
        let data = new Data();
        try {
           let search =  req.body[oMsg.SEARCH];
           console.log(req.body);
           console.log(search);
           search = data.setLikesObject(search);
           console.log(search);
           let query =  OrganizacaoDAO.daoMongo.find(search);
            await data.createPagination(req, query);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoListarOrganizacoes);
            console.log("Erro: " + error);
        } finally {
            res.status(200).json(data);
        }
    }

    public async getAllFilter(req, res){
        let data = new Data();
        try {
            let search =  req.body[oMsg.SEARCH];
            data.setLikesObject(search);
            data.list = await OrganizacaoDAO.daoMongo.find(search);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoListarOrganizacoes);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getAll(req, res){
        let data = new Data();
        try {
            data.list = await OrganizacaoDAO.daoMongo.find({});
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoListarOrganizacoes);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async create(req, res){
        let data = new Data();
        try {
            let organizacao = req.body;
            data.obj = await OrganizacaoDAO.daoMongo.create(organizacao);
            data.addMsg(tMsg.SUCCESS, msgOrganizacao.organizacaoInseridaComSucesso);
            
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoInserirOrganizacao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async update(req, res){
        let data = new Data();;
        try {
            let id = req.params.id;
            let organizacao = req.body;
            data.obj = await  OrganizacaoDAO.daoMongo.findByIdAndUpdate(id, organizacao);
            data.addMsg(tMsg.SUCCESS, msgOrganizacao.organizacaoAtualizadaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoAtualizarOrganizacao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async remove(req, res){
        let data = new Data();
        try {
            let id = req.params.id;
            data.obj = await OrganizacaoDAO.daoMongo.findByIdAndRemove(id);
            data.addMsg(tMsg.SUCCESS, msgOrganizacao.organizacaoRemovidaComSucesso);
        } catch (error) {
            data.addMsg(tMsg.DANGER, msgOrganizacao.erroAoRemoverOrganizacao);
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }

    public async getById(req, res){
        let data = new Data();
        let id = req.params.id;
        try {
            data.obj = await OrganizacaoDAO.daoMongo.findById(id);
        } catch (error) {
            data[tMsg.DANGER] = msgOrganizacao.erroAoLocalizarOrganizacao;
            console.log("Erro: " + error);
        }finally{
            res.status(200).json(data);
        }
    }
}

export default new OrganizacaoService();